$(document).ready(function () {

    var windowWidth = $(window).width();

    //var navpos = $('#mainnav').offset();

    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 300) {
            $('#mainnav').addClass('navbar-fixed-top');
            //$('.ecart-js').addClass('ecart-top-js');


            // $('.color-js').addClass('narbar-color-fixed');
            // $('.barBtn-js').addClass('narbar-color-fixed');
            // $('.navbar-brand img').attr('src', 'img/logo-dark.png');
            // $('.nav-icon').addClass('btn-fixed');

            // $('.color-js').click(function () {
            //     $('.color-js').removeClass('activeLink-js');
            //     $(this).addClass('activeLink-js');

            //     $('.navbar-collapse').removeClass('show');
            //     $('.nav-icon_1').removeClass('nav-icon--first');
            //     $('.nav-icon_2').removeClass('nav-icon--middle');
            //     $('.nav-icon_3').removeClass('nav-icon--last');
            // });
        } else {
            $('#mainnav').removeClass('navbar-fixed-top');
            //$('.ecart-js').removeClass('ecart-top-js');




            // $('.color-js').removeClass('narbar-color-fixed');
            // $('.barBtn-js').removeClass('narbar-color-fixed');
            // $('.navbar-brand img').attr('src', 'img/logo-transparent.png');
            // $('.nav-icon').removeClass('btn-fixed');
            // $('.color-js').removeClass('activeLink-js');
        }
    });

    //menu js
    $("#bars-js").click(function(){
        $(".navbar__list").addClass("show-js");
        
         $(this).addClass("hide-js")
    })

    $('#close-js').click(function(){
        $('.navbar__list').removeClass('show-js');
        $("#bars-js").removeClass("hide-js")
    });

    document.querySelectorAll('.navbar__item').forEach(elt => {
        elt.addEventListener("click", function(){
            $('.navbar__list').removeClass('show-js');
            $('#bars-js').removeClass('hide-js');
            //alert(elt)
        })
    });



    $('.slider').slick({
        //setting-name: setting-value
        autoplay: true,
        autoplaySpeed: 10000, //4000
        speed: 400,
        dots: true,
        fade: true,
    });

    $('#services').slick({
        //setting-name: setting-value
        autoplay: true,
        autoplaySpeed: 4000, //4000
        speed: 400,
        slidesToShow: 3,
        //dots: true,
        //fade: true,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    // arrows: false,
                    // centerMode: true,
                    // centerPadding: '40px',
                    slidesToShow: 2,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    // arrows: false,
                    // centerMode: true,
                    // centerPadding: '40px',
                    slidesToShow: 1,
                },
            },
        ],
    });

    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 1000) {
            $('.linkToHome').css({
                right: '30px',
            });
        } else {
            $('.linkToHome').css({
                right: '-40px',
            });
        }
    });



    // Smooth Scrolling
	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	});

    
});

 VanillaTilt.init(document.querySelectorAll('.categories__link'), {
     max: 25,
     speed: 400,
     glare: true,
     "max-glare": .5
 });
